#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pop.hub


def start():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="bodger")
    hub.bodger.init.cli()


if __name__ == "__main__":
    start()
