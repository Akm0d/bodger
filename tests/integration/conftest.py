import mock
import pathlib
import pytest


@pytest.fixture(scope="session")
def hub(hub):
    hub.pop.sub.add(dyne_name="bodger")
    path = pathlib.Path(__file__).parent.joinpath("test-build.conf")

    with mock.patch("sys.argv", ["bodger", "test", f"--config={path}", "--timeout=1"]):
        hub.bodger.init.cli()

    yield hub
