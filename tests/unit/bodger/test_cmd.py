import pytest
from dict_tools import data


def test_put(mock_hub, hub):
    mock_hub.bodger.RUNS = hub.bodger.RUNS
    mock_hub.bodger.cmd.put = hub.bodger.cmd.put
    cmd = "test_str"
    mock_hub.bodger.cmd.put(cmd)

    assert cmd == mock_hub.bodger.RUNS.get()


def test_match(hub, mock_hub):
    mock_hub.bodger.cmd.match = hub.bodger.cmd.match

    # Build Grains
    mock_hub.grains.GRAINS = data.NamespaceDict()
    mock_hub.grains.GRAINS.match_grain = "grain_val"

    # Build OPT
    tcmd = "echo tiamat build etc"
    mock_hub.OPT = data.NamespaceDict()
    mock_hub.OPT.bodger = data.NamespaceDict(
        cmd="pkg", pkg={"match_grain:grain_val": tcmd}
    )

    mock_hub.exec.cmd.run.return_value = data.NamespaceDict(retcode=0)

    mock_hub.bodger.cmd.match()


@pytest.mark.asyncio
async def test_run_all(hub, mock_hub):
    mock_hub.bodger.cmd.run_all = hub.bodger.cmd.run_all
    mock_hub.bodger.RUNS = hub.bodger.RUNS
    mock_hub.bodger.RUNS.put("test_cmd")
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict(retcode=0)

    await mock_hub.bodger.cmd.run_all()

    mock_hub.exec.cmd.run.assert_called_once()
