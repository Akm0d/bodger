from dict_tools import data


def test_cli(hub, mock_hub):
    mock_hub.pop.loop.start = hub.pop.loop.start
    mock_hub.bodger.init.cli = hub.bodger.init.cli

    mock_hub.OPT = data.NamespaceDict(bodger=data.NamespaceDict(cmd="test", test={}))

    mock_hub.bodger.init.cli()

    mock_hub.grains.init.standalone.assert_called_once_with()
    mock_hub.bodger.cmd.match.assert_called_once_with()
    mock_hub.bodger.cmd.run_all.assert_called_once_with()
